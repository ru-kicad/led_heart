# led_heart

Blinking LED heart on ATxmega32E5. Device is powered supply is Li-Ion 18650 battery, which can be charged from standar micro USB port.

## KiCAD libraries

Custom symbols and footprints used in project [kikad_ru_libs](https://gitlab.com/ru-kicad/kikad_ru_libs)

## Firmware

Firmware source code for ATxmega32E5: [led-heart](https://gitlab.com/ru-avr/led-heart)

## PCB overview

![PCB front side](3d-view-front.png?raw=true "PCB-overview")
